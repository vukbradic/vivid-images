# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.10.10 (MySQL 5.7.21-0ubuntu0.16.04.1)
# Database: homestead
# Generation Time: 2018-05-06 22:11:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table colors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `colors`;

CREATE TABLE `colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table data_rows
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_rows`;

CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`)
VALUES
	(1,1,'id','number','ID',1,0,0,0,0,0,'',1),
	(2,1,'name','text','Name',1,1,1,1,1,1,'',2),
	(3,1,'email','text','Email',1,1,1,1,1,1,'',3),
	(4,1,'password','password','Password',1,0,0,1,1,0,'',4),
	(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,'',5),
	(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,'',6),
	(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',7),
	(8,1,'avatar','image','Avatar',0,1,1,1,1,1,'',8),
	(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),
	(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),
	(11,1,'locale','text','Locale',0,1,1,1,1,0,'',12),
	(12,2,'id','number','ID',1,0,0,0,0,0,'',1),
	(13,2,'name','text','Name',1,1,1,1,1,1,'',2),
	(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),
	(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),
	(16,3,'id','number','ID',1,0,0,0,0,0,'',1),
	(17,3,'name','text','Name',1,1,1,1,1,1,'',2),
	(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),
	(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),
	(20,3,'display_name','text','Display Name',1,1,1,1,1,1,'',5),
	(21,1,'role_id','text','Role',1,1,1,1,1,1,'',9),
	(22,4,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(23,4,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(24,4,'menu_featured','checkbox','Menu Featured',0,1,1,1,1,1,NULL,3),
	(25,4,'image_path','image','Image',0,1,1,1,1,1,NULL,4),
	(26,4,'description','text_area','Description',0,1,1,1,1,1,NULL,5),
	(27,4,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,6),
	(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),
	(29,5,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(30,5,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(31,5,'product_category_id','select_dropdown','Product Category Id',0,1,1,1,1,1,NULL,3),
	(32,5,'description','text','Description',0,1,1,1,1,1,NULL,4),
	(33,5,'product_type_belongsto_product_category_relationship','relationship','product_categories',0,1,1,1,1,1,'{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"product_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":null}',5),
	(34,12,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(35,12,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(36,12,'description','text','Description',0,1,1,1,1,1,NULL,3),
	(37,12,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),
	(38,12,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),
	(39,14,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(40,14,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(41,14,'description','text','Description',0,1,1,1,1,1,NULL,3),
	(42,14,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),
	(43,14,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),
	(44,15,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(45,15,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(46,15,'description','text','Description',0,1,1,1,1,1,NULL,3),
	(47,15,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),
	(48,15,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),
	(49,16,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(50,16,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(51,16,'description','text','Description',0,1,1,1,1,1,NULL,3),
	(52,16,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),
	(53,16,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),
	(54,17,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(55,17,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(56,17,'stock_category_id','select_dropdown','Stock Category Id',0,1,1,1,1,1,NULL,3),
	(57,17,'description','text','Description',0,1,1,1,1,1,NULL,4),
	(58,17,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,5),
	(59,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),
	(60,17,'stock_type_belongsto_stock_category_relationship','relationship','stock_categories',0,1,1,1,1,1,'{\"model\":\"App\\\\StockCategory\",\"table\":\"stock_categories\",\"type\":\"belongsTo\",\"column\":\"stock_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":null}',7),
	(61,18,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(62,18,'product_category_id','select_dropdown','Product Category',0,1,1,1,1,1,NULL,3),
	(63,18,'product_type_id','select_dropdown','Product Type',0,1,1,1,1,1,NULL,4),
	(64,18,'size_id','select_dropdown','Size',0,1,1,1,1,1,NULL,5),
	(65,18,'color_id','select_dropdown','Color',0,1,1,1,1,1,NULL,6),
	(66,18,'printer_id','select_dropdown','Printer',0,1,1,1,1,1,NULL,7),
	(67,18,'stock_category_id','select_dropdown','Stock Category',0,1,1,1,1,1,NULL,8),
	(68,18,'stock_type_id','select_dropdown','Stock Type',0,1,1,1,1,1,NULL,9),
	(69,18,'number_per_paper','text','Number Per Paper',0,1,1,1,1,1,NULL,10),
	(70,18,'quantity','text','Quantity',0,1,1,1,1,1,NULL,11),
	(71,18,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,12),
	(72,18,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,13),
	(73,18,'variation_belongsto_product_category_relationship','relationship','Product Categories',0,1,1,1,1,1,'{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"product_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),
	(74,18,'variation_belongsto_product_type_relationship','relationship','product_types',0,1,1,1,1,1,'{\"model\":\"App\\\\ProductType\",\"table\":\"product_types\",\"type\":\"belongsTo\",\"column\":\"product_type_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',14),
	(75,18,'variation_belongsto_size_relationship','relationship','sizes',0,1,1,1,1,1,'{\"model\":\"App\\\\Size\",\"table\":\"sizes\",\"type\":\"belongsTo\",\"column\":\"size_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',15),
	(76,18,'variation_belongsto_color_relationship','relationship','colors',0,1,1,1,1,1,'{\"model\":\"App\\\\Color\",\"table\":\"colors\",\"type\":\"belongsTo\",\"column\":\"color_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',16),
	(77,18,'variation_belongsto_printer_relationship','relationship','printers',0,1,1,1,1,1,'{\"model\":\"App\\\\Printer\",\"table\":\"printers\",\"type\":\"belongsTo\",\"column\":\"printer_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',17),
	(78,18,'variation_belongsto_stock_category_relationship','relationship','stock_categories',0,1,1,1,1,1,'{\"model\":\"App\\\\StockCategory\",\"table\":\"stock_categories\",\"type\":\"belongsTo\",\"column\":\"stock_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',18),
	(79,18,'variation_belongsto_stock_type_relationship','relationship','stock_types',0,1,1,1,1,1,'{\"model\":\"App\\\\StockType\",\"table\":\"stock_types\",\"type\":\"belongsTo\",\"column\":\"stock_type_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"colors\",\"pivot\":\"0\",\"taggable\":\"0\"}',19);

/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table data_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_types`;

CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`)
VALUES
	(1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,NULL,'2018-04-29 15:17:04','2018-04-29 15:17:04'),
	(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-04-29 15:17:04','2018-04-29 15:17:04'),
	(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2018-04-29 15:17:04','2018-04-29 15:17:04'),
	(4,'product_categories','product-categories','Product Category','Product Categories',NULL,'App\\ProductCategory',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 15:24:52','2018-04-29 15:24:52'),
	(5,'product_types','product-types','Product Type','Product Types',NULL,'App\\ProductType',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 15:29:55','2018-04-29 15:29:55'),
	(12,'colors','colors','Color','Colors',NULL,'App\\Color',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 15:53:33','2018-04-29 15:53:33'),
	(14,'sizes','sizes','Size','Sizes',NULL,'App\\Size',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 15:59:09','2018-04-29 15:59:09'),
	(15,'printers','printers','Printer','Printers',NULL,'App\\Printer',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 12:35:19','2018-05-03 12:35:19'),
	(16,'stock_categories','stock-categories','Stock Category','Stock Categories',NULL,'App\\StockCategory',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 13:36:03','2018-05-03 13:36:03'),
	(17,'stock_types','stock-types','Stock Type','Stock Types',NULL,'App\\StockType',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 13:39:52','2018-05-03 13:39:52'),
	(18,'variations','variations','Variation','Variations',NULL,'App\\Variation',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 13:49:48','2018-05-03 13:49:48');

/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`)
VALUES
	(3,1,'Users','','_self','voyager-person',NULL,NULL,2,'2018-04-29 15:17:05','2018-05-03 14:08:26','voyager.users.index',NULL),
	(4,1,'Roles','','_self','voyager-lock',NULL,NULL,1,'2018-04-29 15:17:05','2018-05-03 14:08:26','voyager.roles.index',NULL),
	(6,1,'Menu Builder','','_self','voyager-list',NULL,5,2,'2018-04-29 15:17:05','2018-05-03 14:08:56','voyager.menus.index',NULL),
	(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2018-04-29 15:17:05','2018-05-03 14:08:56','voyager.compass.index',NULL),
	(10,1,'Hooks','','_self','voyager-hook',NULL,5,4,'2018-04-29 15:17:05','2018-05-03 14:08:56','voyager.hooks',NULL),
	(11,1,'Product Categories','','_self','voyager-folder','#000000',NULL,6,'2018-04-29 15:24:53','2018-05-03 14:08:56','voyager.product-categories.index','null'),
	(12,1,'Product Types','','_self','voyager-folder','#000000',NULL,7,'2018-04-29 15:29:55','2018-05-03 14:08:56','voyager.product-types.index','null'),
	(13,1,'Colors','','_self',NULL,NULL,15,2,'2018-04-29 15:53:33','2018-04-29 16:01:37','voyager.colors.index',NULL),
	(14,1,'Sizes','','_self',NULL,NULL,15,1,'2018-04-29 15:59:09','2018-04-29 16:01:25','voyager.sizes.index',NULL),
	(15,1,'Variation Attributes','#','_self','voyager-tools','#000000',NULL,8,'2018-04-29 16:00:59','2018-05-03 14:10:57',NULL,''),
	(16,1,'Printers','','_self',NULL,NULL,15,3,'2018-05-03 12:35:19','2018-05-03 12:35:41','voyager.printers.index',NULL),
	(17,1,'Stock Categories','','_self','voyager-data','#000000',NULL,9,'2018-05-03 13:36:03','2018-05-03 14:08:56','voyager.stock-categories.index','null'),
	(18,1,'Stock Types','','_self','voyager-data','#000000',NULL,10,'2018-05-03 13:39:52','2018-05-03 14:08:56','voyager.stock-types.index','null'),
	(19,1,'Variations','','_self','voyager-plus','#000000',NULL,11,'2018-05-03 13:49:48','2018-05-03 14:08:56','voyager.variations.index','null');

/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','2018-04-29 15:17:05','2018-04-29 15:17:05');

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2016_01_01_000000_add_voyager_user_fields',1),
	(4,'2016_01_01_000000_create_data_types_table',1),
	(5,'2016_05_19_173453_create_menu_table',1),
	(6,'2016_10_21_190000_create_roles_table',1),
	(7,'2016_10_21_190000_create_settings_table',1),
	(8,'2016_11_30_135954_create_permission_table',1),
	(9,'2016_11_30_141208_create_permission_role_table',1),
	(10,'2016_12_26_201236_data_types__add__server_side',1),
	(11,'2017_01_13_000000_add_route_to_menu_items_table',1),
	(12,'2017_01_14_005015_create_translations_table',1),
	(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),
	(14,'2017_03_06_000000_add_controller_to_data_types_table',1),
	(15,'2017_04_21_000000_add_order_to_data_rows_table',1),
	(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),
	(17,'2017_08_05_000000_add_group_to_settings_table',1),
	(18,'2017_11_26_013050_add_user_role_relationship',1),
	(19,'2017_11_26_015000_create_user_roles_table',1),
	(20,'2018_03_11_000000_add_user_settings',1),
	(21,'2018_03_14_000000_add_details_to_data_types_table',1),
	(22,'2018_03_16_000000_make_settings_value_nullable',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`permission_id`, `role_id`)
VALUES
	(1,1),
	(1,2),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(27,2),
	(28,1),
	(28,2),
	(29,1),
	(29,2),
	(30,1),
	(30,2),
	(31,1),
	(31,2),
	(32,1),
	(32,2),
	(33,1),
	(33,2),
	(34,1),
	(34,2),
	(35,1),
	(35,2),
	(36,1),
	(36,2),
	(37,1),
	(37,2),
	(38,1),
	(38,2),
	(39,1),
	(39,2),
	(40,1),
	(40,2),
	(41,1),
	(41,2),
	(42,1),
	(42,2),
	(43,1),
	(43,2),
	(44,1),
	(44,2),
	(45,1),
	(45,2),
	(46,1),
	(46,2),
	(47,1),
	(47,2),
	(48,1),
	(48,2),
	(49,1),
	(49,2),
	(50,1),
	(50,2),
	(51,1),
	(51,2),
	(52,1),
	(52,2),
	(53,1),
	(53,2),
	(54,1),
	(54,2),
	(55,1),
	(55,2),
	(56,1),
	(56,2),
	(57,1),
	(57,2),
	(58,1),
	(58,2),
	(59,1),
	(59,2),
	(60,1),
	(60,2),
	(61,1),
	(61,2),
	(62,1),
	(62,2),
	(63,1),
	(63,2),
	(64,1),
	(64,2),
	(65,1),
	(65,2),
	(66,1),
	(66,2);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`)
VALUES
	(1,'browse_admin',NULL,'2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(2,'browse_bread',NULL,'2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(3,'browse_database',NULL,'2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(4,'browse_media',NULL,'2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(5,'browse_compass',NULL,'2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(6,'browse_menus','menus','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(7,'read_menus','menus','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(8,'edit_menus','menus','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(9,'add_menus','menus','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(10,'delete_menus','menus','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(11,'browse_roles','roles','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(12,'read_roles','roles','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(13,'edit_roles','roles','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(14,'add_roles','roles','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(15,'delete_roles','roles','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(16,'browse_users','users','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(17,'read_users','users','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(18,'edit_users','users','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(19,'add_users','users','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(20,'delete_users','users','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(21,'browse_settings','settings','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(22,'read_settings','settings','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(23,'edit_settings','settings','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(24,'add_settings','settings','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(25,'delete_settings','settings','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(26,'browse_hooks',NULL,'2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(27,'browse_product_categories','product_categories','2018-04-29 15:24:53','2018-04-29 15:24:53'),
	(28,'read_product_categories','product_categories','2018-04-29 15:24:53','2018-04-29 15:24:53'),
	(29,'edit_product_categories','product_categories','2018-04-29 15:24:53','2018-04-29 15:24:53'),
	(30,'add_product_categories','product_categories','2018-04-29 15:24:53','2018-04-29 15:24:53'),
	(31,'delete_product_categories','product_categories','2018-04-29 15:24:53','2018-04-29 15:24:53'),
	(32,'browse_product_types','product_types','2018-04-29 15:29:55','2018-04-29 15:29:55'),
	(33,'read_product_types','product_types','2018-04-29 15:29:55','2018-04-29 15:29:55'),
	(34,'edit_product_types','product_types','2018-04-29 15:29:55','2018-04-29 15:29:55'),
	(35,'add_product_types','product_types','2018-04-29 15:29:55','2018-04-29 15:29:55'),
	(36,'delete_product_types','product_types','2018-04-29 15:29:55','2018-04-29 15:29:55'),
	(37,'browse_colors','colors','2018-04-29 15:53:33','2018-04-29 15:53:33'),
	(38,'read_colors','colors','2018-04-29 15:53:33','2018-04-29 15:53:33'),
	(39,'edit_colors','colors','2018-04-29 15:53:33','2018-04-29 15:53:33'),
	(40,'add_colors','colors','2018-04-29 15:53:33','2018-04-29 15:53:33'),
	(41,'delete_colors','colors','2018-04-29 15:53:33','2018-04-29 15:53:33'),
	(42,'browse_sizes','sizes','2018-04-29 15:59:09','2018-04-29 15:59:09'),
	(43,'read_sizes','sizes','2018-04-29 15:59:09','2018-04-29 15:59:09'),
	(44,'edit_sizes','sizes','2018-04-29 15:59:09','2018-04-29 15:59:09'),
	(45,'add_sizes','sizes','2018-04-29 15:59:09','2018-04-29 15:59:09'),
	(46,'delete_sizes','sizes','2018-04-29 15:59:09','2018-04-29 15:59:09'),
	(47,'browse_printers','printers','2018-05-03 12:35:19','2018-05-03 12:35:19'),
	(48,'read_printers','printers','2018-05-03 12:35:19','2018-05-03 12:35:19'),
	(49,'edit_printers','printers','2018-05-03 12:35:19','2018-05-03 12:35:19'),
	(50,'add_printers','printers','2018-05-03 12:35:19','2018-05-03 12:35:19'),
	(51,'delete_printers','printers','2018-05-03 12:35:19','2018-05-03 12:35:19'),
	(52,'browse_stock_categories','stock_categories','2018-05-03 13:36:03','2018-05-03 13:36:03'),
	(53,'read_stock_categories','stock_categories','2018-05-03 13:36:03','2018-05-03 13:36:03'),
	(54,'edit_stock_categories','stock_categories','2018-05-03 13:36:03','2018-05-03 13:36:03'),
	(55,'add_stock_categories','stock_categories','2018-05-03 13:36:03','2018-05-03 13:36:03'),
	(56,'delete_stock_categories','stock_categories','2018-05-03 13:36:03','2018-05-03 13:36:03'),
	(57,'browse_stock_types','stock_types','2018-05-03 13:39:52','2018-05-03 13:39:52'),
	(58,'read_stock_types','stock_types','2018-05-03 13:39:52','2018-05-03 13:39:52'),
	(59,'edit_stock_types','stock_types','2018-05-03 13:39:52','2018-05-03 13:39:52'),
	(60,'add_stock_types','stock_types','2018-05-03 13:39:52','2018-05-03 13:39:52'),
	(61,'delete_stock_types','stock_types','2018-05-03 13:39:52','2018-05-03 13:39:52'),
	(62,'browse_variations','variations','2018-05-03 13:49:48','2018-05-03 13:49:48'),
	(63,'read_variations','variations','2018-05-03 13:49:48','2018-05-03 13:49:48'),
	(64,'edit_variations','variations','2018-05-03 13:49:48','2018-05-03 13:49:48'),
	(65,'add_variations','variations','2018-05-03 13:49:48','2018-05-03 13:49:48'),
	(66,'delete_variations','variations','2018-05-03 13:49:48','2018-05-03 13:49:48');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table printers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `printers`;

CREATE TABLE `printers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `printers` WRITE;
/*!40000 ALTER TABLE `printers` DISABLE KEYS */;

INSERT INTO `printers` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'HP XX-55',NULL,'2018-05-03 13:55:12','2018-05-03 13:55:12'),
	(2,'Samsung 43.2',NULL,'2018-05-03 13:55:30','2018-05-03 13:55:30');

/*!40000 ALTER TABLE `printers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_categories`;

CREATE TABLE `product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `menu_featured` tinyint(4) DEFAULT NULL,
  `image_path` mediumtext COLLATE utf8_unicode_ci,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;

INSERT INTO `product_categories` (`id`, `name`, `menu_featured`, `image_path`, `description`, `created_at`, `updated_at`)
VALUES
	(4,'Nurgle shit',1,'product-categories/April2018/Ux1DKgIFtEuvfQA4ya6b.jpg','nurgle shity shit','2018-04-29 15:36:02','2018-04-29 15:36:02'),
	(5,'Posters&Flyers',1,'product-categories/April2018/peEl2NWqXuRLX28lPtGl.png','Posters and flyers','2018-04-29 15:36:57','2018-04-29 15:36:57');

/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_types`;

CREATE TABLE `product_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product_types` WRITE;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;

INSERT INTO `product_types` (`id`, `name`, `product_category_id`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'Poster',5,'poster desc','2018-04-29 15:39:32','2018-04-29 15:39:32'),
	(2,'Flyer',5,'flyer desc','2018-04-29 15:39:54','2018-04-29 15:39:54'),
	(3,'nnnnn',4,'nnnn desc','2018-04-29 15:40:08','2018-04-29 15:40:08');

/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Administrator','2018-04-29 15:17:05','2018-04-29 15:17:05'),
	(2,'user','Normal User','2018-04-29 15:17:05','2018-04-29 15:17:05');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`)
VALUES
	(1,'site.title','Site Title','Site Title','','text',1,'Site'),
	(2,'site.description','Site Description','Site Description','','text',2,'Site'),
	(3,'site.logo','Site Logo','','','image',3,'Site'),
	(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),
	(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),
	(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),
	(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),
	(8,'admin.loader','Admin Loader','','','image',3,'Admin'),
	(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),
	(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sizes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sizes`;

CREATE TABLE `sizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table stock_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stock_categories`;

CREATE TABLE `stock_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `stock_categories` WRITE;
/*!40000 ALTER TABLE `stock_categories` DISABLE KEYS */;

INSERT INTO `stock_categories` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'Paper',NULL,'2018-05-03 13:36:54','2018-05-03 13:36:54'),
	(2,'Ink',NULL,'2018-05-03 13:37:04','2018-05-03 13:37:04');

/*!40000 ALTER TABLE `stock_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stock_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stock_types`;

CREATE TABLE `stock_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock_category_id` int(11) DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `stock_types` WRITE;
/*!40000 ALTER TABLE `stock_types` DISABLE KEYS */;

INSERT INTO `stock_types` (`id`, `name`, `stock_category_id`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'Small paper',1,NULL,'2018-05-03 13:41:52','2018-05-03 13:41:52'),
	(2,'Black Ink',2,NULL,'2018-05-03 13:42:09','2018-05-03 13:42:09');

/*!40000 ALTER TABLE `stock_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`)
VALUES
	(1,1,'Administrator','admin@vividimages.com','users/May2018/1jZFQCbgEiWXOUwHOW64.png','$2y$10$rcz3RineoHo5oJuxdZwt3.09l8hdTo1jI/eQudwTQYSHaTWaQgksy','nsQFt8G6jsgSVMSNaguqNqV7OW3eiA64gFf0TeVxpaD0RDmM20NFEiST8sLG','{\"locale\":\"en\"}','2018-04-29 15:18:38','2018-05-03 12:47:11'),
	(2,2,'testUser','test@vividimages.com','users/May2018/AQ3TL1ITfWINgNCcvjKl.png','$2y$10$6T9FbnXL0JXTYsgQG02DhOo9/l85t1No135iDcn7189SdwZ4pDSsi','dsyHyGxyihnqwO1Pe4bOTIDWMv0zX0Fj3c4sMxGcj64pQIAIWUx5hmchsW5d','{\"locale\":\"en\"}','2018-05-03 12:41:30','2018-05-03 13:08:03');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table variations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `variations`;

CREATE TABLE `variations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `printer_id` int(11) DEFAULT NULL,
  `stock_category_id` int(11) DEFAULT NULL,
  `stock_type_id` int(11) DEFAULT NULL,
  `number_per_paper` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
