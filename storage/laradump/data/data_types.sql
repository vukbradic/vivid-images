
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,NULL,'2018-04-29 22:17:04','2018-04-29 22:17:04'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-04-29 22:17:04','2018-04-29 22:17:04'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2018-04-29 22:17:04','2018-04-29 22:17:04'),(4,'product_categories','product-categories','Product Category','Product Categories',NULL,'App\\ProductCategory',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 22:24:52','2018-04-29 22:24:52'),(5,'product_types','product-types','Product Type','Product Types',NULL,'App\\ProductType',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 22:29:55','2018-04-29 22:29:55'),(12,'colors','colors','Color','Colors',NULL,'App\\Color',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 22:53:33','2018-04-29 22:53:33'),(14,'sizes','sizes','Size','Sizes',NULL,'App\\Size',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-29 22:59:09','2018-04-29 22:59:09'),(15,'printers','printers','Printer','Printers',NULL,'App\\Printer',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 19:35:19','2018-05-03 19:35:19'),(16,'stock_categories','stock-categories','Stock Category','Stock Categories',NULL,'App\\StockCategory',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 20:36:03','2018-05-03 20:36:03'),(17,'stock_types','stock-types','Stock Type','Stock Types',NULL,'App\\StockType',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 20:39:52','2018-05-03 20:39:52'),(18,'variations','variations','Variation','Variations',NULL,'App\\Variation',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-03 20:49:48','2018-05-03 20:49:48');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

