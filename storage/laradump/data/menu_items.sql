
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (3,1,'Users','','_self','voyager-person',NULL,NULL,2,'2018-04-29 22:17:05','2018-05-03 21:08:26','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,1,'2018-04-29 22:17:05','2018-05-03 21:08:26','voyager.roles.index',NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,2,'2018-04-29 22:17:05','2018-05-03 21:08:56','voyager.menus.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2018-04-29 22:17:05','2018-05-03 21:08:56','voyager.compass.index',NULL),(10,1,'Hooks','','_self','voyager-hook',NULL,5,4,'2018-04-29 22:17:05','2018-05-03 21:08:56','voyager.hooks',NULL),(11,1,'Product Categories','','_self','voyager-folder','#000000',NULL,6,'2018-04-29 22:24:53','2018-05-03 21:08:56','voyager.product-categories.index','null'),(12,1,'Product Types','','_self','voyager-folder','#000000',NULL,7,'2018-04-29 22:29:55','2018-05-03 21:08:56','voyager.product-types.index','null'),(13,1,'Colors','','_self',NULL,NULL,15,2,'2018-04-29 22:53:33','2018-04-29 23:01:37','voyager.colors.index',NULL),(14,1,'Sizes','','_self',NULL,NULL,15,1,'2018-04-29 22:59:09','2018-04-29 23:01:25','voyager.sizes.index',NULL),(15,1,'Variation Attributes','#','_self','voyager-tools','#000000',NULL,8,'2018-04-29 23:00:59','2018-05-03 21:10:57',NULL,''),(16,1,'Printers','','_self',NULL,NULL,15,3,'2018-05-03 19:35:19','2018-05-03 19:35:41','voyager.printers.index',NULL),(17,1,'Stock Categories','','_self','voyager-data','#000000',NULL,9,'2018-05-03 20:36:03','2018-05-03 21:08:56','voyager.stock-categories.index','null'),(18,1,'Stock Types','','_self','voyager-data','#000000',NULL,10,'2018-05-03 20:39:52','2018-05-03 21:08:56','voyager.stock-types.index','null'),(19,1,'Variations','','_self','voyager-plus','#000000',NULL,11,'2018-05-03 20:49:48','2018-05-03 21:08:56','voyager.variations.index','null');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

